=================================== AAX2MP3 ====================================

Description:   AAX2MP3 is a collection of scripts to find an easy way to
               convert Audible audio books (.aax) to MP3.
               The conversion script will convert all .aax files in a given
               input directory to .mp3 in a give output directory. The books
               can be split into chapter (default) giving one MP3 file per
               chapter
               In the output directory a subdirectory per audio book will be
               created based on the album title.
               To allow easy resumption all error free converted .aax files are
               moved to a subdirectory "converted" in the input directory. Just
               reinvoke the conversion script to convert the rest.
               A configuration file will be provided to have a persistent
               configuration.

Users work:    To decode .aax file we need to now the so called activation bytes
               for your personal Audible account
               In the internet you will find suitable ways to extract the
               activation bytes
               The most simple way is to install the Audible (download) manager
               (see https://www.audible.de/ep/apps-computer),
               acitivate it, download one audio book and play the first seconds.
               After activating your device (PC) the activation bytes can be
               derived from Windows Registry as follows. Issue the following
               command (e.g. in PowerShell or CMD)
                  reg query HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Audible\SWGIDMAP

                  You will get an output like this:
                     HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Audible\SWGIDMAP
                      0    REG_BINARY    0281B17D000000000001...9
                      1    REG_BINARY    0281B17D000100000001...2
                      2    REG_BINARY    02B674C9000200000001...C
                      3    REG_BINARY    08BB5D48000300000001...9
                      4    REG_BINARY    02B18034000400000001...D
                      5    REG_BINARY    02695123000500000001...2
                      6    REG_BINARY    0514B078000600000001...4
                      7    REG_BINARY    08A11AF5000700000001...3

                   Take the first 4 bytes of entry "0", in this case 01.81.B1.7D and
                   revert them, in this case 7D.B1.81.01 (the dots are not part and
                   only inserted for better reading :)
                   Your activation byte string in this example is "7DB18102"

                if you have installed the Audible App from Windows Store you can use a hex
                editor (e.g. https://mh-nexus.de/downloads/HxDPortableSetup.zip) of your
                choice to find the activation bytes. The bytes are store in the
                AudibleActivation.sys file of the Audible App. this is locate in a path like
                %appdata%\..\local\Packages\audibleinc.audibleforwindows..\LocalState\AudibleActivation.sys
                  *  invoke the hex editor and open AudibleActivation.sys
                  * the first four bytes in reverse order are your activation key
                     - e.g. you see 02 91 b1 7d as first 4 bytes
                     - the activation_bytes are 7db19102
               If Cygwin is available and the Audible App is installed you can use the command
                  * hexdump /cygdrive/c/Users/$USER/AppData/Local/Packages/AudibleInc.AudibleforWindows*/LocalState/AudibleActivation.sys | head -n 1 | sed -r 's/^.{8}(.{4}) (.{4}).*/\2\1/'
                     - the result are directly your activation bytes

               Another way is to download the rcrack utility from https://github.com/inAudible-NG/tables
                  * create a folder "rcrack"
                  * from folder "run" download to your folder
                     - rcrack.exe, charset.txt and alglib1.dll
                  * create a subfolder named "tables"
                  * install OpenAudible (to extract somne rainbow tables)
                     - copy the .rt files from "C:\Program Files\OpenAudible\bin\tables"
                       to the "rcrack\tables" folder
                  * get the checksum from one of your .aax files
                     - "ffprobe <filename.aax> and look for the line containing
                        "[aax] file checksum ==" and copy the checksum
                  * now use rcrack.exe to calculate your activation bytes
                     - rcrack.exe -h <checksum>
                     - the last line looks like
                        <checksum>  }\xb2\x92\x02  hex:a0bde171
                     the output behind "hex:" are your activation bytes
                  - you can uninstall OpenAudible now, if you don't need it

               You also can use https://github.com/inAudible-NG/audible-activator ,
               but I did not test it yet.

               Refer to a search engine of your choice and look for
                  * audible activation bytes
               to get more choices

Reqirements:   The scripts are Python scripts, so Python is needed. Development
               and testing was done with the Python from Microsofts App Store.
               The recoding will be done by ffmpeg, so you have to install
               ffmpeg, see http://ffmpeg.org/download.html

Installation:  1. Install Python distribution from MS App store
               2. Update pip (Package Installer for Python)
                  pip install –upgrade pip’
               3. install some required python packages
                  pip install easygui
               4. copy the scripts to a folder of your choice

Configuration: A Gui is provided to create a persistent config in the current
               working directory
               Customize the following values:
                  * ffmpeg_path, needed to find the executable ffprobe and ffmpeg
                  * Activation bytes ( no default )
                  * Input directory (no default )
                  * Output directory ( no default )
                  * Chapter split enabled (y/n, default y)
                  * Quality setting (0-9, default is 2)
                     0 - VBR 220-260 (highest possible)
                     1 - VBR 190-250 (very high quality)
                     2 - VBR 170-200 (very good quality)
                     3 - VBR 159-195 (good quality)
                     .
                     .
                     6 - VBR 100-130 (normal quality)
                     .
                     .
                     9 - VBR 45-85 (acceptable for spoken text)
                  * options for ffmpeg calls (REAL EXPERTS ONLY), see source code

Invocation:    Create to shortcuts to aax2mp3_gui.py and to aax2mp3_cmd.py (e.g. on your
               Desktop).
                  aax2mp3_gui.py to change your settings
                  aax2mp3_cmd.py to do recoding

ToDos:         * think about implementing ffmpeg options
                  - Issue to find the correct point to insert. Before "-i" or behind
               * Currently existing files are overwritten, is it necessary to have an
                  option here to avoid? Incomplete files will not be corrected
               * Test on Unix platform, maybe on Raspberry PI? We have to handle the
                  settings in another way, a GUI is not guaranteed on Unix.
