#! /usr/bin/env python

import sys, getopt, os, glob, re, pathlib, subprocess, time, shutil, yaml
# from easygui import *
from subprocess import *
from pathlib import *

# print("started in", os.getcwd() )

# print("script located at", sys.path[0] )

# In Windows, when started via doubleclick, the current working directory
# is not correct, even when using a shortcut. So we chdir to the script
# location
x = os.chdir( sys.path[0] )

settingsFilename = "settings.aax2mp3"

## retrieve settings
settingsFilename = "aax2mp3.yaml"
with open(settingsFilename, encoding="utf-8") as cfgfile:
   config = yaml.safe_load(cfgfile)

settings_indir = str(config["config"]["indir"])
settings_outdir = str(config["config"]["outdir"])
settings_split = str(config["config"]["split"])
settings_bitrate = str(config["config"]["bitrate"])
settings_ffmpegopt = str(config["config"]["ffmpegopt"])
settings_ffmpeg_path = str(config["config"]["ffmpeg_path"])
settings_activationbytes = str(config["config"]["activationbytes"])

# settings_activationbytes="7db19101"

ffmpeg = settings_ffmpeg_path + os.sep + "ffmpeg.exe"
ffprobe = settings_ffmpeg_path + os.sep + "ffprobe.exe"
ffmpeg = str(pathlib.Path(ffmpeg))
ffprobe = str(pathlib.Path(ffprobe))

# Flow is as follows:
#     get a list of all .aax files in settings_indir
#     foreach file in the list
#        check the activation bytes
#        create a subdirectory for the output using the album title
#        if chapter split is enabled
#           get the list of chapters
#           recode each chapter using ffmpeg
#        else
#           recode the whole file

# Get a list of .aax files in input directory
files = glob.glob(settings_indir + "/*.aax")
if len(files) == 0:
   print("No AAX files in", settings_indir)
   print("\tPress <ENTER> to continue:")
   input()
   exit(False)

# process each file
for file in files:
   print("====================== processing:",file)

   # Check if activation_bytes are fine
   command = [ ffprobe, "-activation_bytes", settings_activationbytes, "-i", file,\
             "-print_format", "csv", "-show_chapters", "-loglevel", "quiet"]
   output = ""
   # ffprobe checks the activation bytes ever only the chapters are printed.
   # I found no other way to check the activation bytes
   activation_error = subprocess.call(command, stdout=DEVNULL, stderr=DEVNULL, encoding='utf-8', universal_newlines=True)
   if activation_error == 1:
      print("Your activation bytes", settings_activationbytes, "are invalid")
      break

   # just get Album title, no encoding will happen here
   command = [ ffmpeg, "-activation_bytes", settings_activationbytes, "-n", "-i", file, "-c", "copy", "-map_metadata", "0", "-map_metadata:s:a", "0:s:a",\
   "-f", "ffmetadata", "-loglevel", "quiet", "-" ]
   output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   # as each chapter has a separate title, only the first found title is for the album
   title = ""
   for line in iter(output.splitlines()):
      if title != "":
         break
      m = re.match("^title=(.*)", line)
      if m != None:
         # Evaluate title, replace invalid characters (":","/","\") by " _ "
         # to avoid problems when constructing the subfolder name in settings_outdir
         title = re.sub(' +', ' ', re.sub(r"[:?/\\]", "._", m.group(1)))
      # print("Title is:", title)

   # create subdirectory (title) in settings_outdir
   dirname = str(pathlib.Path(settings_outdir + os.sep + title))
   # print(dirname)

   # create output directory, if needed
   if not os.path.isdir(dirname):
      x = os.mkdir(dirname)

   # get cover, this is necessary for performance reasons. Omit the "-vn" option from
   # ffmpeg recoding leads to a significant performance degredation.
   cover = dirname + os.sep + "cover.png"
   with open(os.devnull, "w") as f:
    subprocess.call([ffmpeg, "-y", "-i", file, "-loglevel", "quiet", cover], stdout=f)

   # use ffmpeg to decode the .aax file
   #  tested with an 4-core/8-thread Core i8 with 2.8 GHz, input and output disk on SSD
   #     source: 230 minutes aax best quality
   #     target: 230 minutes mpr VBR 170-210 ( -qscale:a 3 )
   #     elapsed: 250 seconds
   #     performance: encode 1 minute in 0.92 seconds, 1 hour in 55.2 seconds
   #     rough estimation: 1 hour input recoded in 1 minute
   if settings_split == "y":
      # print("decode with chapters")

      # get chapter information
      command = [ ffprobe, "-loglevel", "error", "-activation_bytes", settings_activationbytes, "-i", file, "-print_format", "csv", "-show_chapters" ]
      # When processing the command output utf-8 encoding is needed to handle e.g. German Umlaut
      output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
      chapters = []
      count = 0
      for line in iter(output.splitlines()):
         count += 1
         fields = line.split(",")
         # collect track title, start, end, track number
         chapters.append( { "name": str("00" + str(count))[-3:] + " - " + title, "start": fields[4], "end": fields[6], "track": str("00" + str(count))[-3:] } )

      # convert each chapter to MP3 with filename as title and the preserve the album title
      #   When -ss occurs before -i, it is treated as instructions for the input,
      #     so the first frame of the video stream is the one at 30 seconds.
      #   When -ss occurs after -i, it is treated as an effect, and the input file is always
      #     read from beginning. The unwanted of frames are read and dropped,
      #     leading to a significant performance difference.
      #   to avoid loosing the metadata the map_metadata must be specified explictly !
      # For chapterwise conversion the -vn (omit video) as input option is needed for good performnance,
      #  so we have to add the cover as second input file (see -map options)
      #
      start_time = time.time()
      for chapter in chapters:
         ofile = dirname + os.sep + chapter["name"] + ".mp3"
         print("  ======================  processing chapter:", chapter["track"] + " of " + str(count))
         command = [ ffmpeg, "-loglevel", "error", "-stats", "-activation_bytes", settings_activationbytes, "-y", "-vn",\
                     "-ss", chapter["start"], "-to", chapter["end"], \
                     "-i", file, "-i", cover,\
                     "-map", "0:a", "-map", "1:v",\
                     "-map_metadata", "0", "-id3v2_version", "3", \
                     "-ar", "48000", "-acodec", "libmp3lame", "-ac", "2", "-qscale:a", settings_bitrate, \
                     "-metadata", "title=" + chapter["name"], "-metadata", "track=" + chapter["track"], \
                     ofile ]
         # print( command)
         rc = 0
         process = subprocess.Popen(command,\
                        stdout=subprocess.PIPE,\
                        encoding="utf-8",\
                        universal_newlines=True)
         # print("\n")
         print_lines = False
         while True:
            output = process.stdout.readline()
            # drop a lot of empty lines
            if output.strip() != "": print("    " + output.strip())
            return_code = process.poll()
            if return_code is not None:
               # print('    RETURN CODE', return_code)
               rc += return_code
               # Process has finished, read rest of the output
               for output in process.stdout.readlines():
                  # drop a lot of empty lines
                  if output.strip() != "": print("    " + output.strip())
               break

      duration = time.time() - start_time
      print(" ====================== end of processing: return code was ", rc, "elapsed time in seconds:",  duration)
   else:
      ofile = dirname + os.sep + title + ".mp3"
      # Copy the whole file including metadata like title/album/cover etc.
      command = [ ffmpeg, "-loglevel", "error", "-stats", "-activation_bytes", settings_activationbytes, "-y", "-i", file, \
                     "-map_metadata", "0", "-id3v2_version", "3",\
                     "-acodec", "libmp3lame", "-ac", "2", "-qscale:a", settings_bitrate,\
                     "-ar", "48000",\
                     ofile ]
      start_time = time.time()
      # print(command)
      rc = 0
      process = subprocess.Popen(command,\
                        stdout=subprocess.PIPE,\
                        encoding="utf-8",\
                        universal_newlines=True)

      # print("\n")
      print_lines = False
      while True:
         output = process.stdout.readline()
         # drop a lot of empty lines
         if output.strip() != "": print(output.strip())
         return_code = process.poll()
         if return_code is not None:
            print('RETURN CODE', return_code)
            rc += return_code
            # Process has finished, read rest of the output
            for output in process.stdout.readlines():
               # drop a lot of empty lines
               if output.strip() != "": print(output.strip())
            break

      duration = time.time() - start_time
      print(" ====================== end of processing: return code was ", rc, \
         "elapsed time in seconds:",  duration)

   if rc == 0:
      donedir = settings_indir + os.sep + "converted"
      if not os.path.isdir(donedir):
         x = os.mkdir(donedir)
      x = shutil.move(file,donedir + os.sep)
      print(file, "was moved to", x)

print("\tPls. check the output and press enter to finish")
input()


exit()
