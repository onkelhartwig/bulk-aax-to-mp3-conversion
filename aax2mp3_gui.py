import PySimpleGUI as sg
import os, sys, yaml, ruamel.yaml, pathlib, re

# In Windows, when started via doubleclick, the current working directory
# is not correct, even when using a shortcut. So we chdir to the script
# location
x = os.chdir( sys.path[0] )

# retrieve settings from config file
settingsFilename = "aax2mp3.yaml"
empty_config = False
try:
   with open(settingsFilename, encoding="utf-8") as cfgfile:
      config = yaml.safe_load(cfgfile)
except FileNotFoundError:
   empty_config = True
if empty_config:
   settings_indir = "C:/Audible"
   settings_outdir = "C:/MP3"
   settings_split = "y"
   settings_bitrate = "4"
   settings_ffmpegopt = ""
   settings_ffmpeg_path = "C:/ffmpeg/bin"
   settings_activationbytes = "........"
else:
   settings_indir = str(config["config"]["indir"])
   settings_outdir = str(config["config"]["outdir"])
   settings_split = str(config["config"]["split"])
   settings_bitrate = str(config["config"]["bitrate"])
   settings_ffmpegopt = str(config["config"]["ffmpegopt"])
   settings_ffmpeg_path = str(config["config"]["ffmpeg_path"])
   settings_activationbytes = str(config["config"]["activationbytes"])

# Testing: Give a popup of available themes
# sg.preview_all_look_and_feel_themes(columns=12)

# Define static texts
Head = "Change your AAX2MP3 settings"
Indir = "Specify AAX input directory"
Outdir = "Specify MP3 output directory"
Split = "Split output into chapters"
Quality = "Output quality (0=Best, 9=speech, 4=recommended)"
Ffmpegopt = "Options for ffmpeg (not yet implemented)"
Ffmpeg_path = "Path to ffmpeg executable"
Activationbytes = "Your activation bytes"

# Display Settings GUI until "Cancel" or "Submit" has been pressed
repeat = True
while repeat:
   rc = 0
   sg.ChangeLookAndFeel("DarkBlue12")
   form = sg.FlexForm('AAX2MP3 settings', default_element_size=(40, 1))
   # To make it easier to find the correct directory we look for ffmpeg.exe instead just for a folder. The
   #  stored settings will only contain the path
   settings_ffmpeg_path = settings_ffmpeg_path + "/ffmpeg.exe"
   # to avoid "None" to be displayed for ffmpegopt None will be converted to ""
   if settings_ffmpegopt == "None":
      settings_ffmpegopt = ""
   # Define GUI Layout (each line in square bracket)
   layout = [
      [ sg.Text(Head) ],
      [ sg.Text("_"  * 80) ],
      [ sg.Text(Indir, size=(38, 1)), sg.InputText(settings_indir), sg.FolderBrowse(initial_folder=settings_indir) ],
      [ sg.Text(Outdir, size=(38, 1)), sg.InputText(settings_outdir), sg.FolderBrowse(initial_folder=settings_outdir) ],
      [ sg.Text(Split, size=(38, 1)), sg.Spin(values=("y", "n"), initial_value=settings_split) ],
      [ sg.Text(Quality, size=(38, 1)), sg.Slider(range=(0, 9), orientation='h', size=(10, 20), default_value=settings_bitrate) ],
      [ sg.Text(Ffmpegopt, size=(38, 1)), sg.InputText(settings_ffmpegopt) ],
      [ sg.Text(Ffmpeg_path, size=(38, 1)), sg.InputText(settings_ffmpeg_path), sg.FileBrowse(file_types=(("Exectutables", "ffmpeg*"),),
                                                                                              initial_folder=os.path.dirname(settings_ffmpeg_path)) ],
      [ sg.Text(Activationbytes, size=(38, 1)), sg.InputText(settings_activationbytes) ],
      [ sg.Submit(), sg.Cancel() ]
            ]
   # Get input from GUI
   button, values = form.Layout(layout).Read()
   # sg.MsgBox(button, values)
   if button == "Submit":
      settings_indir = str(values.get(0))
      settings_outdir = str(values.get(1))
      settings_split = str(values.get(2))
      settings_bitrate = str(int(values.get(3)))
      settings_ffmpegopt = str(values.get(4))
      settings_ffmpeg_path = os.path.dirname(str(values.get(5)))
      settings_activationbytes = str(values.get(6))
      # Now we have to check all settings
      # indir
      if not os.path.isdir(settings_indir):
         rc +=1
         sg.popup_error("settings_indir " + settings_indir + " does not exist or is not a directory", line_width=80)
      # outdir
      elif not os.path.isdir(settings_outdir):
         rc +=1
         sg.popup_error("settings_outdir " + settings_outdir + " does not exist or is not a directory", line_width=80)
      # split
      elif settings_split not in [ "y", "n" ]:
         rc +=1
         sg.popup_error("settings_split " + settings_split + " is neither 'y' nor 'n'", line_width=80)
      # bitrate
      elif settings_bitrate not in [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ]:
         rc +=1
         sg.popup_error("settings_bitrate " + settings_bitrate + " is not a single digit", line_width=80)
      # ffmpegopt will not be checked
      elif settings_ffmpegopt == None:
         settings_ffmpegopt = ""
      # ffmpeg_path
      elif not os.path.isdir(settings_ffmpeg_path):
         rc +=1
         sg.popup_error("settings_ffmpeg_path " + settings_ffmpeg_path + " does not exist or is not a directory", line_width=80)
      elif len([x for x in os.listdir(pathlib.Path(settings_ffmpeg_path)) if re.search("ff(mpeg|probe)", x)]) < 2:
         rc += 1
         sg.popup_error("settings_ffmpeg_path " + settings_ffmpeg_path + " does not contain both, ffmpeg and ffprobe", line_width=80)
      # activationbytes (8 hex characters)
      elif re.search('^[0-9A-Fa-f]{8,8}$',settings_activationbytes) == None:
         rc +=1
         sg.popup_error("settings_activationbytes " + settings_activationbytes + " is not a valid 8 character hex string")
      # if no error was found we can leave the GUI
      if rc == 0:
         repeat = False
      # Destroy layout instance
      form = form.Close()
   else:
      repeat = False

# Parameters are checked, if submit was pressed ask to store the settings
if rc == 0 and button == "Submit":
   x = sg.PopupYesNo('Do you want to save your settings?')
   if x ==  "Yes":
      # Save settings as yaml file
      out = """\
      config:
         # AAX Input directory
         indir          : %s
         # MP3 Output directory
         outdir         : %s
         # Split chapters (y/n)
         split          : %s
         # Output quality (0=best, 9=worse), see readme.txt
         bitrate        : %s
         # Not yet implemented
         ffmpegopt      : %s
         # Path to ffmpeg and ffprobe executable
         ffmpeg_path    : %s
         # Audible activation_bytes, see readme.txt
         activationbytes: %s
      """
      out = out % (settings_indir, \
                   settings_outdir, \
                   settings_split, \
                   settings_bitrate,\
                   settings_ffmpegopt, \
                   settings_ffmpeg_path, \
                   settings_activationbytes
                   )
      # data = ruamel.yaml.round_trip_load(out)
      # settings = ruamel.yaml.round_trip_dump(data)
      # with open(settingsFilename, mode="wt", encoding="utf-8") as f:
          # ruamel.yaml.round_trip_dump(data, f)
      yml=YAML()
      data = yml.load(out)
      yml.dump(data, pathlib.Path(settingsFilename))
# All done
exit()
