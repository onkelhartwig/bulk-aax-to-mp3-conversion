# extract activation_bytes from Audible App (Windows Store)

# needed for hex conversion
import binascii 
# needed for file handling
import os
# needed to find the correct absolute path
import glob
# needed to normalioze the path 
import pathlib

# Windows Apps ae located here
appdata = str(pathlib.Path(os.environ["APPDATA"] + os.sep + ".." + os.sep + "/Local/Packages/"))
# We are looking for the AudibleActivation.sys file of the Audible App
searchfor = appdata + "/AudibleInc.AudibleforWindowsPhone_*/LocalState/AudibleActivation.sys"

try:
   # get the exact filename
   fn = pathlib.Path(glob.glob(searchfor)[0])
except IndexError:
   # error handling if Auible App is not installed or not activated
   print("AudibleActivation.sys cant be located.")
   print("  searched in", searchfor)
   print("\tPress Enter to continue")
   input()
   exit(False)

# read the first 4 byte
with open(fn, "rb") as fd:
   a = fd.read(4)
   fd.close
# revert the sequence (ATTENTION: you can't do that in place, a=a[::-1] will not work)
b=a[::-1]
# convert to hex
c=str(binascii.hexlify(b))

# Output will be "b'01020304'", so we have to strip first two and the last character
print("Found activation_bytes =", c[2:-1])
print("\tPress Enter to continue")
input()
exit(True)
