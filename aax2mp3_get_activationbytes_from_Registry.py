import subprocess, re

# Read all subkeys and their values from HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Audible\SWGIDMAP

key="HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Audible\SWGIDMAP"

try:
   result = subprocess.run([\
                           "reg",\
                           "query",\
                           key\
                           ],\
                           stdout=subprocess.PIPE,\
                           check=True\
                          )
except subprocess.CalledProcessError:
   print("REG command give non-zero return code")
   exit(False)

# Output is one string with \r\n as line delimiter
out = result.stdout.decode('utf-8')
# Split in single lines
outlines = out.split("\r\n")

# Search the lines for activation_bytes. Each relevant line looks like
#     0    REG_BINARY    0291B17D0000000000010000 (140 character string)
act_num = 0
for line in outlines:
   # is their a 140 character string of hex data in the line
   relevant = re.match("(.*REG_BINARY *)([0-9A-F]{140})", line)
   if relevant:
      act_num += 1
      # We got a relevant line, only the first 4 bytes (8 characters) are needed
      act_forward=relevant.group(2)[0:8]
      # now we have to revert the line (reading from end to start)
      act_correct=act_forward[6:8] + \
               act_forward[4:6] + \
               act_forward[2:4] + \
               act_forward[0:2]
      print(act_correct)

if act_num == 0:
   print("No activation_bytes found")
else:
   print("Found", act_num, "possible activation_bytes, try from first to last")
   
print("\tPress Enter to continue")
input()
 